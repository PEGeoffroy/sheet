# TODO

* [ ] Importer les icones FA concerné (peut-être en gérant bien le gitignore)
* [ ] Faire des liens vers le SRD pour les termes exacts
* [ ] Attribution (FA, H&D, D&D, [DP](https://top.gg/bot/279722369260453888))
* [ ] Faire un générateur input > json (faire import + export pour les modifications)
* [ ] Citer le boilerplate
* [ ] Copier le parse dans le press papier
* [ ] Générer un dé au hasard
* [ ] Trouver une licence
* [ ] Faire une doc (fr/eng)
* [ ] Faire des commentaires de functions
* [ ] Faire une vision termiunal avec les lancer de dés effectué et une scrollbar
* [ ] Faire des differentes info, des cartes
