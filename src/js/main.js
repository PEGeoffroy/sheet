"use strict";

let main = document.getElementById("main");
let home = document.getElementById("home");
let page = "menu";

let characters = ["hjalkram", "udin"];

async function asyncCall(name) {
    let response = await fetch(`./src/data/${name}.json`);
    let data = await response.json();
    return data;
};

function displayHome() {
    main.appendChild(addIcon("dragon", "s", 4));
    characters.forEach(character => {
        let p = createElement("p", character, "tab");
        p.addEventListener("click", () => {
            asyncCall(character)
                .then(data => {
                    clean();
                    switchPage();
                    home.style.display = "block";
                    displayElement("h1", `${data.firstname} ${data.lastname}`);
                    displayElement("h2", `${data.class} - ${data.archetype} de niveau ${data.level}`);
                    let section = document.createElement("section");
                    section.appendChild(createElement("h3", 'Armes'));
                    data.weapon.forEach(weapon => {
                        let div = document.createElement("div");
                        div.appendChild(createElement("p", `${weapon.name}`, "name"));

                        let hint = weapon.hint;
                        let subSectionHint = document.createElement("section");
                        let displayHint = createElement("p", `(${hint.diceNumber}d${hint.diceType}+${data.char[data.attackChar].value + data.mastery + hint.modifier})`);
                        subSectionHint.appendChild(createElement("p", `Jet de touche`));
                        subSectionHint.appendChild(displayHint);
                        subSectionHint.appendChild(addIcon("dice-d20", "s", 2));
                        subSectionHint.appendChild(createElement("p", "DP", "parser"));

                        let damage = weapon.damage;
                        let subSectionDamage = document.createElement("section");
                        let displayDamage = createElement("p", `(${damage.diceNumber}d${damage.diceType}+${data.char[data.attackChar].value + damage.modifier})`);
                        subSectionDamage.appendChild(createElement("p", `Jet de dégât`));
                        subSectionDamage.appendChild(displayDamage);
                        subSectionDamage.appendChild(addIcon("bolt", "", 2));
                        subSectionDamage.appendChild(createElement("p", "DP", "parser"));
                        
                        div.appendChild(subSectionHint);
                        div.appendChild(subSectionDamage);
                        section.appendChild(div);
                    });
                    main.appendChild(section);
                    console.log(data);
                })
        }, false);
        main.appendChild(p);
    });
}

/**
 * Create an HTML element
 * @param {string} type The type of HTML element we want to create (h*, p).
 * @param {string} text its content.
 * @param {string} addClass to possibly add a class.
 * @return {HTMLElement} The result return an HTML element with the content
 */
function createElement(type, text, addClass = "") {
    let element = document.createElement(type);
    element.innerHTML = text;
    if (addClass !== "") element.setAttribute("class", `${addClass}`);
    return element;
}

function displayElement(type, text, addClass = "") {
    main.appendChild(createElement(type, text, addClass));
}

function clean() {
    main.innerHTML = "";
}

function switchPage() {
    let reverse = (page === "menu") ? "sheet" : "menu";
    main.classList.remove(page);
    main.setAttribute("class", reverse);
    page = reverse;
}

function addIcon(name, special = "", size = 1) {
    let p = document.createElement("p");
    let i = document.createElement("i");
    p.setAttribute("class", "icon");
    i.setAttribute("class", `fa${special} fa-${size}x fa-${name}`);
    p.appendChild(i);
    return p;
}

home.addEventListener("click", () => {
    clean();
    switchPage();
    home.style.display = "none";
    displayHome();
}, false);

displayHome();